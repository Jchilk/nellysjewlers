package com.example.nellysjewelersapp.presenter

import com.example.nellysjewelersapp.detailsActivity.detailsModel.DetailsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel
import retrofit2.Response

interface GetHomeListPresenter {
    fun showError(message: String)
    fun onSuccess(listItemOneProduct : Response<List<ItemsModel>>)
    fun onSuccessItemTwo(listItemTwoProduct : Response<List<ItemsModel>>)
    fun onSuccessItemThree(listItemThreeProduct : Response<List<ItemsModel>>)
    fun onSuccessItemFour(listItemFourProduct : Response<List<ItemsModel>>)
    fun onSuccessItemFive(listItemFiveProduct : Response<List<ItemsModel>>)
    fun onSuccessOffersOne(listOffersOneProduct : Response<List<ItemsOffersModel>>)
    fun onSuccessOffersTwo(listOffersTwoProduct : Response<List<ItemsOffersModel>>)
    fun onSuccessOffersthree(listOffersThreeProduct : Response<List<ItemsOffersModel>>)
    fun onSuccessDetailsList(listDetailsList: Response<List<DetailsModel>>)

        interface getProductPresenter{
            fun onGetListItemOne()
            fun onGetListItemTwo()
            fun onGetListItemThree()
            fun onGetListItemFour()
            fun onGetListItemFive()
            fun onGetListOffersOne()
            fun onGetListOffersTwo()
            fun onGetListOffersThree()
            fun onGetListDetails()
            fun onDestroy()
        }
}