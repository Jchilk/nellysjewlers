package com.example.nellysjewelersapp.presenter

import com.example.nellysjewelersapp.detailsActivity.detailsModel.DetailsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel
import com.example.nellysjewelersapp.interactors.GetHomeListInteractor
import com.example.nellysjewelersapp.interactors.GetHomeListInteractorImpl
import com.example.nellysjewelersapp.views.*

class GetHomeListPresenterImpl: GetHomeListPresenter.getProductPresenter, ListHomeOneView.getListPresenter,
        GetHomeListInteractor.OnGetListHomeItemOneResponseListener, ListHomeTwoView.getListPresenter,
        ListHomeThreeView.getListPresenter,ListHomeFourView.getListPresenter, ListHomeFiveView.getListPresenter,
        ListHomeOffersOneView.getListPresenter, ListHomeOffersTwoView.getListPresenter,
        ListHomeOffersThreeView.getListPresenter, ListDetailsView.getListPresenter{

    var listProductInteractor : GetHomeListInteractor
    lateinit var listProductView : ListHomeOneView
    lateinit var listTwoProductView : ListHomeTwoView
    lateinit var listThreeProductView : ListHomeThreeView
    lateinit var listFourProductView: ListHomeFourView
    lateinit var listFiveProductView: ListHomeFiveView
    lateinit var listOffersOneProductView: ListHomeOffersOneView
    lateinit var listOffersTwoProductView: ListHomeOffersTwoView
    lateinit var listOffersThreeProductView: ListHomeOffersThreeView
    lateinit var listDetailsList : ListDetailsView

    constructor(view: ListHomeOneView){
        listProductView = view
        this.listProductInteractor = GetHomeListInteractorImpl()
    }

    constructor(view: ListHomeTwoView){
        listTwoProductView = view
        this.listProductInteractor = GetHomeListInteractorImpl()
    }

    constructor(view: ListHomeThreeView){
        listThreeProductView = view
        this.listProductInteractor = GetHomeListInteractorImpl()
    }

    constructor(view: ListHomeFourView){
        listFourProductView = view
        this.listProductInteractor = GetHomeListInteractorImpl()
    }

    constructor(view: ListHomeFiveView){
        listFiveProductView = view
        this.listProductInteractor = GetHomeListInteractorImpl()
    }

    constructor(view: ListHomeOffersOneView){
        listOffersOneProductView = view
        this.listProductInteractor = GetHomeListInteractorImpl()
    }

    constructor(view: ListHomeOffersTwoView){
        listOffersTwoProductView = view
        this.listProductInteractor = GetHomeListInteractorImpl()
    }

    constructor(view: ListHomeOffersThreeView){
        listOffersThreeProductView = view
        this.listProductInteractor = GetHomeListInteractorImpl()
    }

    constructor(view: ListDetailsView){
        listDetailsList = view
        this.listProductInteractor = GetHomeListInteractorImpl()
    }

    override fun onGetListItemOne() {
      listProductInteractor.getListItemOneRequest(this)
    }

    override fun onGetListItemTwo() {
        listProductInteractor.getListItemTwoRequest(this)
    }

    override fun onGetListItemThree() {
        listProductInteractor.getListItemThreeRequest(this)
    }

    override fun onGetListItemFour() {
        listProductInteractor.getListItemFourRequest(this)
    }

    override fun onGetListItemFive() {
        listProductInteractor.getListItemFiveRequest(this)
    }

    override fun onGetListOffersOne() {
        listProductInteractor.getListOffersOneRequest(this)
    }

    override fun onGetListOffersTwo() {
        listProductInteractor.getListOffersTwoRequest(this)
    }

    override fun onGetListOffersThree() {
        listProductInteractor.getListOffersThreeRequest(this)
    }

    override fun onGetListDetails() {
        listProductInteractor.getListDetailsListRequest(this)
    }

    override fun onDestroy() {

    }

    override fun onError(e: Throwable) {
        listProductView.onError(e.message)
    }

    override fun onSuccessItemOne(listItemOne: List<ItemsModel>) {
        listProductView.onSuccessItemOne(listItemOne)
    }

    override fun onSuccessItemTwo(listItemTwo: List<ItemsModel>) {
        listTwoProductView.onSuccessItemTwo(listItemTwo)
    }

    override fun onSuccessItemThree(listItemThree: List<ItemsModel>) {
        listThreeProductView.onSuccessItemThree(listItemThree)
    }

    override fun onSuccessItemFour(listItemFour: List<ItemsModel>) {
        listFourProductView.onSuccessItemFour(listItemFour)
    }

    override fun onSuccessItemFive(listItemFive: List<ItemsModel>) {
        listFiveProductView.onSuccessItemFive(listItemFive)
    }

    override fun onSuccessOffersOne(listOffersOne: List<ItemsOffersModel>) {
        listOffersOneProductView.onSuccessOffersOne(listOffersOne)
    }

    override fun onSuccessOffersTwo(listOffersTwo: List<ItemsOffersModel>) {
        listOffersTwoProductView.onSuccessOffersTwo(listOffersTwo)
    }

    override fun onSuccessOffersThree(listOffersThree: List<ItemsOffersModel>) {
        listOffersThreeProductView.onSuccessOffersThree(listOffersThree)
    }

    override fun onSuccessDetailsList(ListDetailsList: List<DetailsModel>) {
        listDetailsList.onSuccessDetailsList(ListDetailsList)
    }

}
