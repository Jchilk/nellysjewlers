package com.example.nellysjewelersapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.nellysjewelersapp.accountFragment.AccountFragment
import com.example.nellysjewelersapp.accountFragment.accountActivity.ProfileFragment
import com.example.nellysjewelersapp.cartFragment.CartFragment
import com.example.nellysjewelersapp.homeFragment.HomeFragment
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeItems.HomeItemsOneFragment
import com.example.nellysjewelersapp.others.addFragment
import com.example.nellysjewelersapp.others.preferences
import com.example.nellysjewelersapp.whislistFragment.WhislistFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        addFragment(HomeFragment(),R.id.main_activity_container)

        bottom_navigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.navigation_home -> addFragment(HomeFragment(),R.id.main_activity_container)
                R.id.navigation_dashboard -> addFragment(CartFragment(),R.id.main_activity_container)
                R.id.navigation_notifications -> if(preferences.readLoginStatus()){
                    addFragment(ProfileFragment(),R.id.main_activity_container)
                }else{
                    addFragment(AccountFragment(),R.id.main_activity_container)
                }
                R.id.navigation_whislist -> addFragment(WhislistFragment(),R.id.main_activity_container)
            }
            true
        }

    }
}
