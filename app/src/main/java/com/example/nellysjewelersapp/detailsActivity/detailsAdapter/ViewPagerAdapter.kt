package com.example.nellysjewelersapp.detailsActivity.detailsAdapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.nellysjewelersapp.detailsActivity.imageFragments.*

class ViewPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

    override fun createFragment(position: Int): Fragment {
        var pageFragment: Fragment = PageFragment()
        when (position) {
            0 -> pageFragment = ImageOneFragment()
            1 -> pageFragment = ImageTwoFragment()
            2 -> pageFragment = ImageThreeFragment()
            3 -> pageFragment = ImageFourFragment()
            4 -> pageFragment = ImageFiveFragment()
        }
        return pageFragment
    }

    override fun getItemCount(): Int {
        return 5
    }
}