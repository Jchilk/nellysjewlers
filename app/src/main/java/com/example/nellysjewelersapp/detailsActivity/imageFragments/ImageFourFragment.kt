package com.example.nellysjewelersapp.detailsActivity.imageFragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.databinding.FragmentImageFourBinding
import com.squareup.picasso.Picasso

class ImageFourFragment : Fragment() {

    private var binding : FragmentImageFourBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentImageFourBinding.inflate(inflater,container,false)
        val view = binding!!.root

        var intent = activity!!.intent.getStringExtra("image")
        Picasso.get().load(intent).into(binding!!.imageviewFragmentFour)

        return view
    }
}