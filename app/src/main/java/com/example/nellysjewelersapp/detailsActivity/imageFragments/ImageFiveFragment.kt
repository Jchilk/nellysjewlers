package com.example.nellysjewelersapp.detailsActivity.imageFragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.databinding.FragmentImageFiveBinding
import com.squareup.picasso.Picasso

class ImageFiveFragment : Fragment() {

    private var binding : FragmentImageFiveBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentImageFiveBinding.inflate(inflater,container,false)
        val view = binding!!.root

        var intent = activity!!.intent.getStringExtra("image")
        Picasso.get().load(intent).into(binding!!.imageviewFragmentFive)

        return view

    }
}