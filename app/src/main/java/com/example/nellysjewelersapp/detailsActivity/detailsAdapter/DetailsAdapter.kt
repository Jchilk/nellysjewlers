package com.example.nellysjewelersapp.detailsActivity.detailsAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.detailsActivity.detailsModel.DetailsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.others.inflate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_detail_layout.view.*

class DetailsAdapter (private var dataList: List<DetailsModel>, private val context: Context) :
        RecyclerView.Adapter<DetailsAdapter.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_detail_layout))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(dataList[position])

    override fun getItemCount() = dataList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bind(detailsModel: DetailsModel) = with(itemView){
            textView_name_item_detail_layout.text = detailsModel.name
            textView_cutted_item_details_layout.text = detailsModel.cuttedPrice
            textView_price_item_details_layout.text = detailsModel.price
            Picasso.get().load(detailsModel.image).into(image_item_detail_layout)

        }
    }
}