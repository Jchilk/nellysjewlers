package com.example.nellysjewelersapp.detailsActivity.imageFragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.databinding.FragmentImageOneBinding
import com.squareup.picasso.Picasso

class ImageOneFragment : Fragment() {

    private var binding : FragmentImageOneBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       binding = FragmentImageOneBinding.inflate(inflater, container,false)
       val view = binding?.root

        val intent = activity?.intent?.getStringExtra("image")
        Picasso.get().load(intent).into(binding!!.imageviewFragmentOne)

       return view
    }
}