package com.example.nellysjewelersapp.detailsActivity.imageFragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.databinding.FragmentImageTwoBinding
import com.squareup.picasso.Picasso
import kotlin.math.PI


class ImageTwoFragment : Fragment() {

    private var binding : FragmentImageTwoBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentImageTwoBinding.inflate(inflater,container,false)
        val view = binding!!.root

        val intent = activity!!.intent.getStringExtra("image")
        Picasso.get().load(intent).into(binding!!.imageviewFragmentTwo)

        return view
    }

}