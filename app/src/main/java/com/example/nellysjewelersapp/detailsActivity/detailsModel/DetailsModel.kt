package com.example.nellysjewelersapp.detailsActivity.detailsModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DetailsModel(

    @SerializedName("id")
    @Expose
    val id: String,

    @SerializedName("image")
    @Expose
    val image: String,

    @SerializedName("name")
    @Expose
    val name: String,

    @SerializedName("prec")
    @Expose
    val price: String,

    @SerializedName("cuttedPrec")
    @Expose
    val cuttedPrice: String

)