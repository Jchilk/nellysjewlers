package com.example.nellysjewelersapp.detailsActivity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.databinding.ActivityDetailsBinding
import com.example.nellysjewelersapp.detailsActivity.bottomSheet.BottomSheetFragment
import com.example.nellysjewelersapp.detailsActivity.detailsAdapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        binding!!.viewPagerDetails.offscreenPageLimit = 1
        var viewPagerAdapter = ViewPagerAdapter(this)
        binding!!.viewPagerDetails.adapter = viewPagerAdapter
        binding!!.viewPagerDetails.setCurrentItem(0)
        binding!!.indicator.setViewPager(viewPager_details)

        var name = intent.getStringExtra("name")
        var price = intent.getStringExtra("price")
        binding!!.textviewNameDetails.text = name
        binding!!.textviewPriceDetails.text = price

        binding!!.containerBottomSheet.setOnClickListener {

            val fragmentBottomSheetFull = BottomSheetFragment()
            fragmentBottomSheetFull.show(supportFragmentManager, fragmentBottomSheetFull.getTag()
            )
        }
        binding!!.detailsShareButton.setOnClickListener {

            val myIntent = Intent(Intent.ACTION_SEND)
            myIntent.type = "type/pelin"
            val shareBoy = "You are body"
            val shareSub = "You subject here"
            myIntent.putExtra(Intent.EXTRA_SUBJECT,shareBoy)
            myIntent.putExtra(Intent.EXTRA_TEXT,shareSub)
            startActivity(Intent.createChooser(myIntent, "Share your app"))
        }

    }
}