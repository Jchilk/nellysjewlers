package com.example.nellysjewelersapp.detailsActivity.bottomSheet

import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.detailsActivity.detailsAdapter.DetailsAdapter
import com.example.nellysjewelersapp.detailsActivity.detailsModel.DetailsModel
import com.example.nellysjewelersapp.others.toast
import com.example.nellysjewelersapp.presenter.GetHomeListPresenterImpl
import com.example.nellysjewelersapp.views.ListDetailsView
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_bottom_sheet.*
import java.util.ArrayList


class BottomSheetFragment : BottomSheetDialogFragment(), ListDetailsView {


    // Inflate the layout for this fragment
    var appBarLayout: AppBarLayout? = null
    var linearLayout: LinearLayout? = null
    var recyclerViewHorizontal: RecyclerView? = null
    lateinit var presenter : ListDetailsView.getListPresenter
    var dataList = ArrayList<DetailsModel>()



    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        val view = View.inflate(context, R.layout.fragment_bottom_sheet, null)
        dialog.setContentView(view)

        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(view.parent as View)
        presenter = GetHomeListPresenterImpl(this)

        bottomSheetBehavior.peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO
        appBarLayout = view.findViewById(R.id.appBarLayout)
        linearLayout = view.findViewById(R.id.lyt_linear)
        hideView(appBarLayout)

        val named = view.findViewById<TextView>(R.id.textview_name_bottom_sheet)
        val priced = view.findViewById<TextView>(R.id.textview_price_bottom_sheets)
        val description = view.findViewById<TextView>(R.id.textview_desc_bottom_sheet)
        val desclarge = view.findViewById<TextView>(R.id.textview_desclar_bottom_sheet)
        recyclerViewHorizontal = view.findViewById(R.id.recyclerview_horizontal_bottom_sheet)

        recyclerViewHorizontal?.adapter =  activity?.let { DetailsAdapter(dataList, it) }
        recyclerViewHorizontal?.layoutManager= LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false)
        presenter.onGetListDetails()

        var name = activity!!.intent.getStringExtra("name")
        var price = activity!!.intent.getStringExtra("price")
        var desc = activity!!.intent.getStringExtra("desc")
        var descLar = activity!!.intent.getStringExtra("desclarg")
        named?.text = name
        priced?.text = price
        description?.text = desc
        desclarge?.text = Html.fromHtml(descLar)

        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(view: View, i: Int) {
                if (BottomSheetBehavior.STATE_EXPANDED == i) {
                    showView(appBarLayout, actionBarSize)
                    hideView(linearLayout)
                }
                if (BottomSheetBehavior.STATE_COLLAPSED == i) {
                    hideView(appBarLayout)
                    showView(linearLayout, actionBarSize)
                }
                if (BottomSheetBehavior.STATE_HIDDEN == i) {
                    dismiss()
                }
            }

            override fun onSlide(view: View, v: Float) {}
        })
        view.findViewById<View>(R.id.closeSheet)
            .setOnClickListener { dismiss() }


        return dialog
    }

    private fun hideView(view: View?) {
        val params = view!!.layoutParams
        params.height = 0
        view.layoutParams = params
    }

    private fun showView(view: View?, size: Int) {
        val params = view!!.layoutParams
        params.height = size
        view.layoutParams = params
    }

    private val actionBarSize: Int
        private get() {
            val styledAttributes =
                context!!.theme.obtainStyledAttributes(intArrayOf(android.R.attr.actionBarSize))
            return styledAttributes.getDimension(0, 0f).toInt()
        }

    override fun onError(e: String?) {
        if(e!!.isEmpty() != null){
            toast(e)
        }
    }

    override fun onSuccessDetailsList(ListDetailsList: List<DetailsModel>) {
        if (ListDetailsList.size != 0){
            dataList.addAll(ListDetailsList)
            recyclerViewHorizontal?.adapter!!.notifyDataSetChanged()
        }
    }

    override fun showProgress(message: String) {
        TODO("Not yet implemented")
    }

    override fun hideProgress() {
        TODO("Not yet implemented")
    }
}