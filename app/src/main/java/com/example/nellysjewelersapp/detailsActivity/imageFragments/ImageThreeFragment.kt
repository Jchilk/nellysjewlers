package com.example.nellysjewelersapp.detailsActivity.imageFragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.databinding.FragmentImageThreeBinding
import com.squareup.picasso.Picasso

class ImageThreeFragment : Fragment() {

    private var binding : FragmentImageThreeBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentImageThreeBinding.inflate(inflater,container,false)
        val view = binding!!.root

        var intent = activity!!.intent.getStringExtra("image")
        Picasso.get().load(intent).into(binding!!.imageviewFragmentThree)

        return view
    }
}