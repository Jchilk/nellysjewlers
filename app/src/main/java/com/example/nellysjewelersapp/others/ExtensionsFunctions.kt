package com.example.nellysjewelersapp.others

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextUtils.replace
import android.text.TextWatcher
import android.util.Patterns
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.example.nellysjewelersapp.R
import java.util.regex.Pattern

fun Activity.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) = Toast.makeText(this, message, duration).show()
fun Fragment.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) = Toast.makeText(context, message, duration).show()

inline fun <reified T: Activity>Activity.goToActivity(noinline init: Intent.() -> Unit = {}){
    val intent = Intent(this, T::class.java)
    intent.init()
    startActivity(intent)
}

inline fun <reified T: Activity>Fragment.goToActivity(noinline init: Intent.() -> Unit = {}){
    val intent = Intent(activity, T::class.java)
    intent.init()
    activity!!.startActivity(intent)
}

fun EditText.validate(validation: (String) -> Unit ){
    this.addTextChangedListener(object: TextWatcher {
        override fun afterTextChanged(editable: Editable) {
            validation(editable.toString())
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }
    } )
}



fun Activity.isValidateEmail(email: String):Boolean{
    val pattern = Patterns.EMAIL_ADDRESS
    return  pattern.matcher(email).matches()
}

fun Activity.isValidatePassword(password: String):Boolean{
    // Necesita Contener ->  1 Num    /  1 Minuscula / 1 Mayuscula / 1 Especial/ Min caracteres 4
    val passwordPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"
    val pattern = Pattern.compile(passwordPattern)
    return  pattern.matcher(password).matches()
}
fun Activity.isValidateConfirmPassword(password: String, confirmPassword: String):Boolean{
    return password == confirmPassword
}

fun AppCompatActivity.addFragment(fragment: Fragment, layout:Int){
    supportFragmentManager.beginTransaction().apply {
        replace(layout, fragment)
        commit()
    }
}

fun Fragment.addFragment(fragment: Fragment, layout:Int){
    activity!!.supportFragmentManager.beginTransaction().apply {
        replace(layout, fragment)
        commit()
    }
}

fun ViewGroup.inflate(layoutId: Int) = LayoutInflater.from(context).inflate(layoutId,this,false)!!








