package com.example.nellysjewelersapp.others

import android.content.Context
import android.content.SharedPreferences


class SharedPreference(val context: Context) {
        private val PREFS_NAME = "com.example.nellysjewelersapp.SHARED_PREFERENCE"
        val sharedPref: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)


    fun saveStatusLogin(status: Boolean) {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putBoolean("statusLoginGoogle", status!!)

        editor.commit()
    }

    fun readLoginStatus( defaultValue: Boolean = false): Boolean{

        return sharedPref.getBoolean("statusLoginGoogle", defaultValue)

    }
}
