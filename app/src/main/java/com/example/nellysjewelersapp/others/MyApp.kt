package com.example.nellysjewelersapp.others

import android.app.Application

val preferences:SharedPreference by lazy { MyApp.sharedPref!! }

class MyApp : Application(){

    companion object {

        var sharedPref: SharedPreference? = null

    }

    override fun onCreate() {
        super.onCreate()
        sharedPref = SharedPreference(applicationContext)
    }

}