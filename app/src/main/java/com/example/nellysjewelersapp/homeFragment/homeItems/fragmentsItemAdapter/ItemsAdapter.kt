package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsItemAdapter

import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.detailsActivity.DetailsActivity
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.others.inflate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_layout.view.*


class ItemsAdapter(private var dataList: List<ItemsModel>, private val context: Context) :
    RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_layout))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(dataList[position])

    override fun getItemCount() = dataList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(itemsModel: ItemsModel) = with(itemView){
            textView_name_item_layout.text = itemsModel.name
            textView_price_item_layout.text = itemsModel.price
            Picasso.get().load(itemsModel.image).into(image_item_layout)

            setOnClickListener {adapterPosition
                val intent = Intent(it.context, DetailsActivity::class.java)
                intent.putExtra("image", itemsModel.image)
                intent.putExtra("name", itemsModel.name)
                intent.putExtra("price", itemsModel.price)
                intent.putExtra("desc",itemsModel.desc)
                intent.putExtra("desclarg", itemsModel.descLarg)
                it.context.startActivity(intent)

            }
        }
    }
}

