package com.example.nellysjewelersapp.homeFragment.homeItems

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.cartFragment.CartFragment
import com.example.nellysjewelersapp.databinding.ActivityHomeItemsBinding
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeItems.*
import com.example.nellysjewelersapp.others.addFragment
import com.example.nellysjewelersapp.others.toast

class HomeItemsActivity : AppCompatActivity() {

    private lateinit var binding : ActivityHomeItemsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var binding = ActivityHomeItemsBinding.inflate(layoutInflater)
        passToActivity()

        binding!!.imageViewBackArrowItemsActivity.setOnClickListener { finish() }
        binding!!.imageviewCartActivityHome.setOnClickListener { addFragment(CartFragment(),R.id.container_home_items) }

        setContentView(binding.root)

    }

    fun passToActivity(){
        var bundle: Bundle? = intent.extras
        if(bundle != null){
            if(bundle.getString("cosmeticList") != null){
                addFragment(HomeItemsOneFragment(),R.id.container_home_items)
            }else   if(bundle.getString("accessoriesList") != null){
                addFragment(HomeItemsTwoFragment(),R.id.container_home_items)
            }else   if(bundle.getString("bagsList") != null){
                addFragment(HomeItemsThreeFragment(),R.id.container_home_items)
            }else   if(bundle.getString("ladyList") != null){
                addFragment(HomeItemsFourFragment(),R.id.container_home_items)
            }else   if(bundle.getString("gentlemenList") != null){
                addFragment(HomeItemsFiveFragment(),R.id.container_home_items)
            }else   if(bundle.getString("offersOneList") != null){
                addFragment(HomeOffersOneFragment(),R.id.container_home_items)
            }else   if(bundle.getString("offersTwoList") != null){
                addFragment(HomeOffersTwoFragment(),R.id.container_home_items)
            }else   if(bundle.getString("offersThreeList") != null){
                addFragment(HomeOffersThreeFragment(),R.id.container_home_items)
            }
        }
    }
}