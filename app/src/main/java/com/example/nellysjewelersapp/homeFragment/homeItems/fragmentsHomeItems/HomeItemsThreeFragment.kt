package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeItems

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.nellysjewelersapp.databinding.FragmentHomeItemsThreeBinding
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsItemAdapter.ItemsAdapter
import com.example.nellysjewelersapp.others.toast
import com.example.nellysjewelersapp.presenter.GetHomeListPresenterImpl
import com.example.nellysjewelersapp.views.ListHomeThreeView
import java.util.ArrayList


class HomeItemsThreeFragment : Fragment(), ListHomeThreeView {

    private var binding:FragmentHomeItemsThreeBinding? = null
    var dataList = ArrayList<ItemsModel>()
    lateinit var presenter : ListHomeThreeView.getListPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeItemsThreeBinding.inflate(inflater,container,false)
        val view = binding?.root

        presenter = GetHomeListPresenterImpl(this)

        binding!!.recyclerViewItemThree.adapter =  activity?.let { ItemsAdapter(dataList, it) }
        binding!!.recyclerViewItemThree.layoutManager = GridLayoutManager(activity,2)
        binding!!.progressBarThree.visibility = View.VISIBLE

        presenter.onGetListItemThree()

        return view
    }

    override fun onError(e: String?) {
        if (e!!.isEmpty()){
            toast(e)
        }
    }

    override fun onSuccessItemThree(listItemThree: List<ItemsModel>) {
      if (listItemThree.size != 0){
          binding!!.progressBarThree.visibility = View.GONE
          dataList.addAll(listItemThree)
          binding!!.recyclerViewItemThree.adapter!!.notifyDataSetChanged()
      }
    }

    override fun showProgress(message: String) {
        TODO("Not yet implemented")
    }

    override fun hideProgress() {
        TODO("Not yet implemented")
    }
}