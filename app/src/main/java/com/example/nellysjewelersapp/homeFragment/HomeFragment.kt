package com.example.nellysjewelersapp.homeFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.nellysjewelersapp.MainActivity
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.cartFragment.CartFragment
import com.example.nellysjewelersapp.databinding.FragmentHomeBinding
import com.example.nellysjewelersapp.homeFragment.homeItems.HomeItemsActivity
import com.example.nellysjewelersapp.others.addFragment
import com.example.nellysjewelersapp.others.goToActivity
import com.example.nellysjewelersapp.others.toast
import com.example.nellysjewelersapp.searchActivity.SearchActivity

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    private var binding:FragmentHomeBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater,container,false)
        val view = binding?.root

        var btnOffers = view!!.findViewById<Button>(R.id.button_image_offers_one)
        var btnOffersTwo = view!!.findViewById<Button>(R.id.button_image_offers_two)
        var btnOffersThree = view!!.findViewById<Button>(R.id.button_image_offers_three)

        binding!!.imageviewHomeBtnSearch.setOnClickListener { goToActivity<SearchActivity>() }
        binding!!.imageviewBagBtnSearch.setOnClickListener { addFragment(CartFragment(),R.id.main_activity_container)}
        btnOffers.setOnClickListener { goToActivity<HomeItemsActivity> {putExtra("offersOneList","")} }
        btnOffersTwo.setOnClickListener { goToActivity<HomeItemsActivity> { putExtra("offersTwoList","") } }
        btnOffersThree.setOnClickListener { goToActivity<HomeItemsActivity> {putExtra("offersThreeList","")  } }
        binding!!.containerHomeImageOne.setOnClickListener { goToActivity<HomeItemsActivity>() {putExtra("cosmeticList","")} }
        binding!!.containerHomeImageTwo.setOnClickListener { goToActivity<HomeItemsActivity>() {putExtra("accessoriesList","")} }
        binding!!.containerHomeImageThree.setOnClickListener { goToActivity<HomeItemsActivity>() {putExtra("bagsList","")} }
        binding!!.containerHomeImageFour.setOnClickListener { goToActivity<HomeItemsActivity>() {putExtra("ladyList","")} }
        binding!!.containerHomeImageFive.setOnClickListener { goToActivity<HomeItemsActivity> { putExtra("gentlemenList","") } }

        return view
    }

}
