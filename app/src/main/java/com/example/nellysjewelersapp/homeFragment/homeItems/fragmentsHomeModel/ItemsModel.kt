package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class ItemsModel  (

    @SerializedName("id")
    @Expose
    val id: String,

    @SerializedName("image")
    @Expose
    val image: String,

    @SerializedName("name")
    @Expose
    val name: String,

    @SerializedName("prec")
    @Expose
    val price: String,

    @SerializedName("desc")
    @Expose
    val desc: String,

    @SerializedName("desclarg")
    @Expose
    val descLarg: String
)