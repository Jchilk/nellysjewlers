package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ItemsOffersModel  (
    @SerializedName("id")
    @Expose
    val id: String,

    @SerializedName("image")
    @Expose
    val image: String,

    @SerializedName("name")
    @Expose
    val name: String,

    @SerializedName("prec")
    @Expose
    val price: String,

    @SerializedName("cuttedPrec")
    @Expose
    val cuttedPrice: String

)