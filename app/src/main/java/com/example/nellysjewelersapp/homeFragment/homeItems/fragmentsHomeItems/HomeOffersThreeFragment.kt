package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeItems

import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.apiClient.ApiClient
import com.example.nellysjewelersapp.databinding.FragmentHomeOffersThreeBinding
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsItemAdapter.ItemsOffersAdapter
import com.example.nellysjewelersapp.others.toast
import com.example.nellysjewelersapp.presenter.GetHomeListPresenterImpl
import com.example.nellysjewelersapp.views.ListHomeOffersOneView
import com.example.nellysjewelersapp.views.ListHomeOffersThreeView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class HomeOffersThreeFragment : Fragment(), ListHomeOffersThreeView {

    private var binding : FragmentHomeOffersThreeBinding? = null
    var dataList = ArrayList<ItemsOffersModel>()
    lateinit var presenter : ListHomeOffersThreeView.getListPresenter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeOffersThreeBinding.inflate(inflater,container,false)
        val view = binding?.root

        presenter = GetHomeListPresenterImpl(this)

        binding!!.recyclerViewItemOffersThree.adapter =  activity?.let { ItemsOffersAdapter(dataList, it) }
        binding!!.recyclerViewItemOffersThree.layoutManager = GridLayoutManager(activity,2)
        binding!!.progressBarOffersThree.visibility = View.VISIBLE

        presenter.onGetListOffersThree()

        return view
    }

    override fun onError(e: String?) {
        if(e!!.isEmpty()){
            toast(e)
        }
    }

    override fun onSuccessOffersThree(listOffersThree: List<ItemsOffersModel>) {
        if(listOffersThree.size != 0){
            binding!!.progressBarOffersThree.visibility = View.GONE
            dataList.addAll(listOffersThree)
            binding!!.recyclerViewItemOffersThree.adapter!!.notifyDataSetChanged()
        }
    }

    override fun showProgress(message: String) {
        TODO("Not yet implemented")
    }

    override fun hideProgress() {
        TODO("Not yet implemented")
    }

}