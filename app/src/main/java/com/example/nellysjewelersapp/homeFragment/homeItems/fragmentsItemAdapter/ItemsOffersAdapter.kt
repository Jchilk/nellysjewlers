package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsItemAdapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.detailsActivity.DetailsActivity
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel
import com.squareup.picasso.Picasso


class ItemsOffersAdapter(private var dataList: List<ItemsOffersModel>, private val context: Context) : RecyclerView.Adapter<ItemsOffersAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_offers_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel=dataList.get(position)

        holder.titleTextView.text=dataModel.name
        holder.priceTextView.text=dataModel.price
        holder.cuttedTextView.text=dataModel.cuttedPrice
        Picasso.get().load(dataList.get(position).image).into(holder.imageList)
    }


    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var titleTextView: TextView
        var priceTextView: TextView
        var cuttedTextView: TextView
        var imageList: ImageView

        init { titleTextView=itemLayoutView.findViewById(R.id.textView_name_item_offers_layout)
            priceTextView = itemLayoutView.findViewById(R.id.textView_price_item_offers_layout)
            cuttedTextView = itemLayoutView.findViewById(R.id.textView_cutted_item_offers_layout)
            imageList = itemLayoutView.findViewById(R.id.image_item_offers_layout)

            itemLayoutView.setOnClickListener {
                val intent = Intent(it.context,DetailsActivity::class.java)
                it.context.startActivity(intent)
            }

        }

    }

}
