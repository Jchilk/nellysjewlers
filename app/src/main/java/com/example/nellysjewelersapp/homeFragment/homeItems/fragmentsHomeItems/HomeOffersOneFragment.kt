package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeItems

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.nellysjewelersapp.apiClient.ApiClient
import com.example.nellysjewelersapp.databinding.FragmentHomeOffersOneBinding
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsItemAdapter.ItemsOffersAdapter
import com.example.nellysjewelersapp.others.toast
import com.example.nellysjewelersapp.presenter.GetHomeListPresenterImpl
import com.example.nellysjewelersapp.views.ListHomeFourView
import com.example.nellysjewelersapp.views.ListHomeOffersOneView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList


class HomeOffersOneFragment : Fragment(), ListHomeOffersOneView {

    private var binding:FragmentHomeOffersOneBinding? = null
    var dataList = ArrayList<ItemsOffersModel>()
    lateinit var presenter : ListHomeOffersOneView.getListPresenter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeOffersOneBinding.inflate(inflater,container,false)
        val view = binding?.root

        presenter = GetHomeListPresenterImpl(this)

        binding!!.recyclerViewItemOffersOne.adapter =  activity?.let { ItemsOffersAdapter(dataList, it) }
        binding!!.recyclerViewItemOffersOne.layoutManager = GridLayoutManager(activity,2)
        binding!!.progressBarOffersOne.visibility = View.VISIBLE

        presenter.onGetListOffersOne()

        return view

    }

    override fun onError(e: String?) {
        if(e!!.isEmpty()){
            toast(e)
        }
    }

    override fun onSuccessOffersOne(listOffersOne: List<ItemsOffersModel>) {
        if(listOffersOne.size != 0){
            binding!!.progressBarOffersOne.visibility = View.GONE
            dataList.addAll(listOffersOne)
            binding!!.recyclerViewItemOffersOne.adapter!!.notifyDataSetChanged()
        }
    }

    override fun showProgress(message: String) {
        TODO("Not yet implemented")
    }

    override fun hideProgress() {
        TODO("Not yet implemented")
    }
}