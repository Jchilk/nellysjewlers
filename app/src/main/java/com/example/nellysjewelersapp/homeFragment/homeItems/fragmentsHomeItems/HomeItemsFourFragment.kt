package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeItems

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.nellysjewelersapp.databinding.FragmentHomeItemsFourBinding
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsItemAdapter.ItemsAdapter
import com.example.nellysjewelersapp.others.toast
import com.example.nellysjewelersapp.presenter.GetHomeListPresenterImpl
import com.example.nellysjewelersapp.views.ListHomeFourView
import java.util.ArrayList


class HomeItemsFourFragment : Fragment(), ListHomeFourView {

    private var binding:FragmentHomeItemsFourBinding? = null
    var dataList = ArrayList<ItemsModel>()
    lateinit var presenter : ListHomeFourView.getListPresenter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeItemsFourBinding.inflate(inflater,container,false)
        val view = binding?.root

        presenter = GetHomeListPresenterImpl(this)

        binding!!.recyclerViewItemFour.adapter =  activity?.let { ItemsAdapter(dataList, it) }
        binding!!.recyclerViewItemFour.layoutManager = GridLayoutManager(activity,2)
        binding!!.progressBarFour.visibility = View.VISIBLE

        presenter.onGetListItemFour()

        return view
    }

    override fun onError(e: String?) {
        if (e!!.isEmpty()){
            toast(e)
        }
    }

    override fun onSuccessItemFour(listItemFour: List<ItemsModel>) {
        if (listItemFour.size != 0){
            binding!!.progressBarFour.visibility = View.GONE
            dataList.addAll(listItemFour)
            binding!!.recyclerViewItemFour.adapter!!.notifyDataSetChanged()
        }
    }

    override fun showProgress(message: String) {
        TODO("Not yet implemented")
    }

    override fun hideProgress() {
        TODO("Not yet implemented")
    }

}