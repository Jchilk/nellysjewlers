package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeItems

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.nellysjewelersapp.databinding.FragmentHomeOffersTwoBinding
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsItemAdapter.ItemsOffersAdapter
import com.example.nellysjewelersapp.others.toast
import com.example.nellysjewelersapp.presenter.GetHomeListPresenterImpl
import com.example.nellysjewelersapp.views.ListHomeOffersTwoView
import java.util.ArrayList

class HomeOffersTwoFragment : Fragment(), ListHomeOffersTwoView {

    private var binding:FragmentHomeOffersTwoBinding? = null
    var dataList = ArrayList<ItemsOffersModel>()
    lateinit var presenter : ListHomeOffersTwoView.getListPresenter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeOffersTwoBinding.inflate(inflater,container,false)
        val view = binding?.root

        presenter = GetHomeListPresenterImpl(this)


        binding!!.recyclerViewItemOffersTwo.adapter =  activity?.let { ItemsOffersAdapter(dataList, it) }
        binding!!.recyclerViewItemOffersTwo.layoutManager = GridLayoutManager(activity,2)
        binding!!.progressBarOffersTwo.visibility = View.VISIBLE

        presenter.onGetListOffersTwo()

        return view
    }

    override fun onError(e: String?) {
        if (e!!.isEmpty()){
            toast(e)
        }
    }

    override fun onSuccessOffersTwo(listOffersTwo: List<ItemsOffersModel>) {
        if(listOffersTwo.size != 0){
            binding!!.progressBarOffersTwo.visibility = View.GONE
            dataList.addAll(listOffersTwo)
            binding!!.recyclerViewItemOffersTwo.adapter!!.notifyDataSetChanged()
        }
    }

    override fun showProgress(message: String) {
        TODO("Not yet implemented")
    }

    override fun hideProgress() {
        TODO("Not yet implemented")
    }
}