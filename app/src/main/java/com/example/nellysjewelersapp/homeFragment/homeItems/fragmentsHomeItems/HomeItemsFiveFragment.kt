package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeItems

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.nellysjewelersapp.databinding.FragmentHomeItemsFiveBinding
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsItemAdapter.ItemsAdapter
import com.example.nellysjewelersapp.others.toast
import com.example.nellysjewelersapp.presenter.GetHomeListPresenterImpl
import com.example.nellysjewelersapp.views.ListHomeFiveView
import java.util.ArrayList

class HomeItemsFiveFragment : Fragment(), ListHomeFiveView {

    private var binding: FragmentHomeItemsFiveBinding? = null
    var dataList = ArrayList<ItemsModel>()
    lateinit var presenter : ListHomeFiveView.getListPresenter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeItemsFiveBinding.inflate(inflater,container,false)
        val view = binding?.root

        presenter = GetHomeListPresenterImpl(this)

        binding!!.recyclerViewItemFive.adapter =  activity?.let { ItemsAdapter(dataList, it) }
        binding!!.recyclerViewItemFive.layoutManager = GridLayoutManager(activity,2)
        binding!!.progressBarFive.visibility = View.VISIBLE

        presenter.onGetListItemFive()

        return view
    }

    override fun onError(e: String?) {
        if(e!!.isEmpty()){
            toast(e)
        }
    }

    override fun onSuccessItemFive(listItemFive: List<ItemsModel>) {
        if (listItemFive.size != 0){
            binding!!.progressBarFive.visibility = View.GONE
            dataList.addAll(listItemFive)
            binding!!.recyclerViewItemFive.adapter!!.notifyDataSetChanged()
        }
    }

    override fun showProgress(message: String) {
        TODO("Not yet implemented")
    }

    override fun hideProgress() {
        TODO("Not yet implemented")
    }
}