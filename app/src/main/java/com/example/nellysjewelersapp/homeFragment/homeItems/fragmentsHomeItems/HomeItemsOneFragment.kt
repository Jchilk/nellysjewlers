package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeItems

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.nellysjewelersapp.databinding.FragmentHomeItemsOneBinding
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsItemAdapter.ItemsAdapter
import com.example.nellysjewelersapp.others.toast
import com.example.nellysjewelersapp.presenter.GetHomeListPresenterImpl
import com.example.nellysjewelersapp.views.ListHomeOneView
import java.util.*

class HomeItemsOneFragment : Fragment(), ListHomeOneView{

    private var binding:FragmentHomeItemsOneBinding? = null
    var dataList = ArrayList<ItemsModel>()
    lateinit var presenter : ListHomeOneView.getListPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeItemsOneBinding.inflate(inflater,container,false)
        val view = binding?.root

        presenter = GetHomeListPresenterImpl(this)


        //setting up the adapter
        binding!!.recyclerViewItemOne.adapter= activity?.let { ItemsAdapter(dataList, it) }
        //recyclerView.layoutManager=LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false)
        binding!!.recyclerViewItemOne.layoutManager= GridLayoutManager(activity,2)
        binding!!.progressBarOne.visibility = View.VISIBLE

        presenter.onGetListItemOne()

        return view
    }

    override fun onError(e: String?) {
        if (e!!.isEmpty()){
            toast(e)
        }
    }

    override fun onSuccessItemOne(listItemOne: List<ItemsModel>) {
        if (listItemOne.size != 0){
            binding!!.progressBarOne.visibility = View.GONE
            dataList.addAll(listItemOne)
            binding!!.recyclerViewItemOne.adapter!!.notifyDataSetChanged()
        }
    }

    override fun showProgress(message: String) {

    }

    override fun hideProgress() {

    }


}



