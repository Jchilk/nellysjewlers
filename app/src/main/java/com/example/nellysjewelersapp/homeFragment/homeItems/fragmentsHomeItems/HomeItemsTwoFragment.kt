package com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeItems

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.nellysjewelersapp.databinding.FragmentHomeItemsTwoBinding
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsItemAdapter.ItemsAdapter
import com.example.nellysjewelersapp.others.toast
import com.example.nellysjewelersapp.presenter.GetHomeListPresenterImpl
import com.example.nellysjewelersapp.views.ListHomeTwoView
import java.util.ArrayList


class HomeItemsTwoFragment : Fragment(), ListHomeTwoView {

    private var binding:FragmentHomeItemsTwoBinding? = null
    var dataList = ArrayList<ItemsModel>()
    lateinit var presenter : ListHomeTwoView.getListPresenter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeItemsTwoBinding.inflate(inflater,container,false)
        val view = binding?.root

        presenter = GetHomeListPresenterImpl(this)

        binding!!.recyclerVIewItemTwo.adapter= activity?.let { ItemsAdapter(dataList, it) }
        binding!!.recyclerVIewItemTwo.layoutManager= GridLayoutManager(activity,2)
        binding!!.progressBarTwo.visibility = View.VISIBLE

        presenter.onGetListItemTwo()

        return view
    }

    override fun onError(e: String?) {
        if (e!!.isEmpty()){
            toast(e)
        }
    }

    override fun onSuccessItemTwo(listItemTwo: List<ItemsModel>) {
        if(listItemTwo.size != 0){
            binding!!.progressBarTwo.visibility = View.GONE
            dataList.addAll(listItemTwo)
            binding!!.recyclerVIewItemTwo.adapter!!.notifyDataSetChanged()
        }
    }

    override fun showProgress(message: String) {

    }

    override fun hideProgress() {

    }

}