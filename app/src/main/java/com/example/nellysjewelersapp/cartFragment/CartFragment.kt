package com.example.nellysjewelersapp.cartFragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.nellysjewelersapp.MainActivity

import com.example.nellysjewelersapp.R

/**
 * A simple [Fragment] subclass.
 */
class CartFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view : View = inflater.inflate(R.layout.fragment_cart, container, false)

        var  btnCar = view.findViewById<Button>(R.id.car_fragment_buy_button)

        btnCar.setOnClickListener {passToMainActivity()}

        return view
    }

    private fun passToMainActivity() {
        val intent = Intent(activity,MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        activity!!.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

    }

}
