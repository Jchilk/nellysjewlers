package com.example.nellysjewelersapp.views

import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel

interface ListHomeThreeView : ProgressHomeDialogView {
    fun onError(e: String?)
    fun onSuccessItemThree(listItemThree: List<ItemsModel>)

    interface getListPresenter{
        fun onGetListItemThree()

    }

}