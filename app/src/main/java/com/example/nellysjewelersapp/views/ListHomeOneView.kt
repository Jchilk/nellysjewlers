package com.example.nellysjewelersapp.views

import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel


interface ListHomeOneView : ProgressHomeDialogView{
    fun onError(e: String?)
    fun onSuccessItemOne(listItemOne: List<ItemsModel>)

    interface getListPresenter{
        fun onGetListItemOne()

    }

}