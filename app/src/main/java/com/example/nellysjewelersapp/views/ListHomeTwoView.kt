package com.example.nellysjewelersapp.views

import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel

interface ListHomeTwoView : ProgressHomeDialogView {
    fun onError(e: String?)
    fun onSuccessItemTwo(listItemTwo: List<ItemsModel>)

    interface getListPresenter{
        fun onGetListItemTwo()

    }

}