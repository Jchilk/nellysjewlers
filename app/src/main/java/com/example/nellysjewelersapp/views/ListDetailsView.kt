package com.example.nellysjewelersapp.views

import com.example.nellysjewelersapp.detailsActivity.detailsModel.DetailsModel

interface ListDetailsView : ProgressHomeDialogView {

    fun onError(e: String?)
    fun onSuccessDetailsList(ListDetailsList: List<DetailsModel>)

    interface getListPresenter{
        fun onGetListDetails()
    }
}