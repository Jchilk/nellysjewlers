package com.example.nellysjewelersapp.views

import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel

interface ListHomeOffersOneView : ProgressHomeDialogView {

    fun onError(e: String?)
    fun onSuccessOffersOne(listOffersOne: List<ItemsOffersModel>)

    interface getListPresenter{
        fun onGetListOffersOne()
    }

}