package com.example.nellysjewelersapp.views

import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel

interface ListHomeFourView : ProgressHomeDialogView{

    fun onError(e: String?)
    fun onSuccessItemFour(listItemFour: List<ItemsModel>)

    interface getListPresenter{
        fun onGetListItemFour()

    }
}