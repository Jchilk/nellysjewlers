package com.example.nellysjewelersapp.views

import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel

interface ListHomeFiveView : ProgressHomeDialogView {

    fun onError(e: String?)
    fun onSuccessItemFive(listItemFive: List<ItemsModel>)

    interface getListPresenter{
        fun onGetListItemFive()
    }
}