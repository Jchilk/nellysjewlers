package com.example.nellysjewelersapp.views

import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel

interface ListHomeOffersTwoView : ProgressHomeDialogView {

    fun onError(e: String?)
    fun onSuccessOffersTwo(listOffersTwo: List<ItemsOffersModel>)

    interface getListPresenter{
        fun onGetListOffersTwo()
    }

}