package com.example.nellysjewelersapp.views

import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel

interface ListHomeOffersThreeView : ProgressHomeDialogView {

    fun onError(e: String?)
    fun onSuccessOffersThree(listOffersThree: List<ItemsOffersModel>)

    interface getListPresenter{
        fun onGetListOffersThree()
    }
}