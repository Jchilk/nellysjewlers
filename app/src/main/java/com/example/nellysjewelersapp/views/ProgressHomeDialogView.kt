package com.example.nellysjewelersapp.views

interface ProgressHomeDialogView {

    fun showProgress(message: String)
    fun hideProgress()

}