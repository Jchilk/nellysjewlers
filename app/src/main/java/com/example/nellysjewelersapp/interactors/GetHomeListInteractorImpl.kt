package com.example.nellysjewelersapp.interactors

import android.view.View
import com.example.nellysjewelersapp.apiClient.ApiClient
import com.example.nellysjewelersapp.detailsActivity.detailsModel.DetailsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel
import com.example.nellysjewelersapp.others.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class GetHomeListInteractorImpl : GetHomeListInteractor{
    override fun getListItemOneRequest(listener: GetHomeListInteractor.OnGetListHomeItemOneResponseListener) {
        val call: Call<List<ItemsModel>> = ApiClient.getClient.getItemOneList()
        call.enqueue(object : Callback<List<ItemsModel>> {

            override fun onResponse(call: Call<List<ItemsModel>>?, response: Response<List<ItemsModel>>?) {
                response?.body()?.let { listener.onSuccessItemOne(it) }
            }

            override fun onFailure(call: Call<List<ItemsModel>>?, t: Throwable?) {
                t?.let { listener.onError(it) }
            }
        })
    }

    override fun getListItemTwoRequest(listener: GetHomeListInteractor.OnGetListHomeItemOneResponseListener) {

            val call: Call<List<ItemsModel>> = ApiClient.getClient.getItemTwoList()
            call.enqueue(object : Callback<List<ItemsModel>> {

                override fun onResponse(call: Call<List<ItemsModel>>?, response: Response<List<ItemsModel>>?) {
                    response?.body()?.let { listener.onSuccessItemTwo(it) }
                }

                override fun onFailure(call: Call<List<ItemsModel>>?, t: Throwable?) {
                    t?.let { listener.onError(it) }
                }
            })
        }

    override fun getListItemThreeRequest(listener: GetHomeListInteractor.OnGetListHomeItemOneResponseListener) {

        val call: Call<List<ItemsModel>> = ApiClient.getClient.getItemThreeList()
        call.enqueue(object : Callback<List<ItemsModel>> {

            override fun onResponse(call: Call<List<ItemsModel>>?, response: Response<List<ItemsModel>>?) {
                response?.body()?.let { listener.onSuccessItemThree(it) }
            }

            override fun onFailure(call: Call<List<ItemsModel>>?, t: Throwable?) {
                t?.let { listener.onError(it) }
            }
        })
    }

    override fun getListItemFourRequest(listener: GetHomeListInteractor.OnGetListHomeItemOneResponseListener) {

        val call: Call<List<ItemsModel>> = ApiClient.getClient.getItemFourList()
        call.enqueue(object : Callback<List<ItemsModel>> {

            override fun onResponse(call: Call<List<ItemsModel>>?, response: Response<List<ItemsModel>>?) {
                response?.body()?.let { listener.onSuccessItemFour(it) }
            }

            override fun onFailure(call: Call<List<ItemsModel>>?, t: Throwable?) {
                t?.let { listener.onError(it) }

            }
        })
    }

    override fun getListItemFiveRequest(listener: GetHomeListInteractor.OnGetListHomeItemOneResponseListener) {

        val call: Call<List<ItemsModel>> = ApiClient.getClient.getItemFiveList()
        call.enqueue(object : Callback<List<ItemsModel>> {

            override fun onResponse(call: Call<List<ItemsModel>>?, response: Response<List<ItemsModel>>?) {
                response?.body()?.let { listener.onSuccessItemFive(it) }
            }

            override fun onFailure(call: Call<List<ItemsModel>>?, t: Throwable?) {
              t?.let { listener.onError(it) }
            }
        })
    }

    override fun getListOffersOneRequest(listener: GetHomeListInteractor.OnGetListHomeItemOneResponseListener) {

        val call: Call<List<ItemsOffersModel>> = ApiClient.getClient.getItemOffersOneList()
        call.enqueue(object : Callback<List<ItemsOffersModel>> {

            override fun onResponse(call: Call<List<ItemsOffersModel>>?, response: Response<List<ItemsOffersModel>>?) {
                response?.body()?.let { listener.onSuccessOffersOne(it) }
            }

            override fun onFailure(call: Call<List<ItemsOffersModel>>?, t: Throwable?) {
                t?.let { listener.onError(t) }
            }
        })
    }

    override fun getListOffersTwoRequest(listener: GetHomeListInteractor.OnGetListHomeItemOneResponseListener) {
        val call: Call<List<ItemsOffersModel>> = ApiClient.getClient.getItemOffersTwoList()
        call.enqueue(object : Callback<List<ItemsOffersModel>> {

            override fun onResponse(call: Call<List<ItemsOffersModel>>?, response: Response<List<ItemsOffersModel>>?) {
                response?.body()?.let { listener.onSuccessOffersTwo(it) }
            }

            override fun onFailure(call: Call<List<ItemsOffersModel>>?, t: Throwable?) {
                t?.let { listener.onError(t) }
            }
        })
    }

    override fun getListOffersThreeRequest(listener: GetHomeListInteractor.OnGetListHomeItemOneResponseListener) {
        val call: Call<List<ItemsOffersModel>> = ApiClient.getClient.getItemOffersThreeList()
        call.enqueue(object : Callback<List<ItemsOffersModel>> {

            override fun onResponse(call: Call<List<ItemsOffersModel>>?, response: Response<List<ItemsOffersModel>>?) {
                response?.body()?.let { listener.onSuccessOffersThree(it) }
            }

            override fun onFailure(call: Call<List<ItemsOffersModel>>?, t: Throwable?) {
                t?.let { listener.onError(it) }
            }
        })

    }

    override fun getListDetailsListRequest(listener: GetHomeListInteractor.OnGetListHomeItemOneResponseListener) {
        val call: Call<List<DetailsModel>> = ApiClient.getClient.getItemHorizontalList()
        call.enqueue(object : Callback<List<DetailsModel>> {

            override fun onResponse(call: Call<List<DetailsModel>>?, response: Response<List<DetailsModel>>?) {
                response?.body()?.let { listener.onSuccessDetailsList(it) }
            }

            override fun onFailure(call: Call<List<DetailsModel>>?, t: Throwable?) {
                t?.let { listener.onError(it) }
            }
        })

    }

}