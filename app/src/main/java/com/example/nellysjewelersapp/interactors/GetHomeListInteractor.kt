package com.example.nellysjewelersapp.interactors

import com.example.nellysjewelersapp.detailsActivity.detailsModel.DetailsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel


interface GetHomeListInteractor {

    interface OnGetListHomeItemOneResponseListener{
           fun onError(e: Throwable)
           fun onSuccessItemOne(listItemOne: List<ItemsModel>)
           fun onSuccessItemTwo(listItemTwo: List<ItemsModel>)
           fun onSuccessItemThree(listItemThree: List<ItemsModel>)
           fun onSuccessItemFour(listItemFour: List<ItemsModel>)
           fun onSuccessItemFive(listItemFive: List<ItemsModel>)
           fun onSuccessOffersOne(listOffersOne: List<ItemsOffersModel>)
           fun onSuccessOffersTwo(listOffersTwo: List<ItemsOffersModel>)
           fun onSuccessOffersThree(listOffersThree: List<ItemsOffersModel>)
           fun onSuccessDetailsList(ListDetailsList: List<DetailsModel>)

    }

    fun getListItemOneRequest(listener: OnGetListHomeItemOneResponseListener)
    fun getListItemTwoRequest(listener: OnGetListHomeItemOneResponseListener)
    fun getListItemThreeRequest(listener: OnGetListHomeItemOneResponseListener)
    fun getListItemFourRequest(listener: OnGetListHomeItemOneResponseListener)
    fun getListItemFiveRequest(listener: OnGetListHomeItemOneResponseListener)
    fun getListOffersOneRequest(listener: OnGetListHomeItemOneResponseListener)
    fun getListOffersTwoRequest(listener: OnGetListHomeItemOneResponseListener)
    fun getListOffersThreeRequest(listener: OnGetListHomeItemOneResponseListener)
    fun getListDetailsListRequest(listener: OnGetListHomeItemOneResponseListener)
}