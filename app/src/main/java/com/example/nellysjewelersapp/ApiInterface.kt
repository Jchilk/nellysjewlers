package com.example.nellysjewelersapp

import com.example.nellysjewelersapp.detailsActivity.detailsModel.DetailsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsModel
import com.example.nellysjewelersapp.homeFragment.homeItems.fragmentsHomeModel.ItemsOffersModel
import retrofit2.Call
import retrofit2.http.GET


interface ApiInterface {

    @GET("pagHome/categoriesList/ofertasRelampago.php")
    fun getItemOneList(): Call<List<ItemsModel>>

    @GET("pagHome/gridItemsList/gridItemOne/gridItemOne.php")
    fun getItemTwoList(): Call<List<ItemsModel>>

    @GET("pagHome/offersWeek/offersWeek.php")
    fun getItemThreeList(): Call<List<ItemsModel>>

    @GET("pagHome/mostWanted/mostWantedLeft.php")
    fun getItemFourList(): Call<List<ItemsModel>>

    @GET("pagHomeGrid/gridThree/gridThree.php")
    fun getItemFiveList(): Call<List<ItemsModel>>

    @GET("pagHome/categoriesList/ofertasRelampago.php")
    fun getItemOffersOneList(): Call<List<ItemsOffersModel>>

    @GET("pagHome/categoriesList/ofertasRelampago.php")
    fun getItemOffersTwoList(): Call<List<ItemsOffersModel>>

    @GET("pagHome/categoriesList/ofertasRelampago.php")
    fun getItemOffersThreeList(): Call<List<ItemsOffersModel>>

    @GET("pagHome/categoriesList/ofertasRelampago.php")
    fun getItemHorizontalList(): Call<List<DetailsModel>>
}