package com.example.nellysjewelersapp.accountFragment.accountActivity

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.example.nellysjewelersapp.MainActivity

import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.others.goToActivity
import com.example.nellysjewelersapp.others.preferences
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_profile.*

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    private val mAuth: FirebaseAuth by lazy { FirebaseAuth.getInstance() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_profile, container, false)

        val btnLogout = view.findViewById<ImageView>(R.id.profile_btnLogout)

        btnLogout.setOnClickListener {

            mAuth!!.signOut()
            LoginManager.getInstance().logOut()
            preferences.saveStatusLogin(false)
            var intent = Intent(activity,MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity!!.startActivity(intent)
            activity!!.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

        }

        return view
    }

}
