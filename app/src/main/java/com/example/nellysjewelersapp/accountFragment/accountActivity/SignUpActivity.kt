package com.example.nellysjewelersapp.accountFragment.accountActivity

import android.content.Intent
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.others.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_up.*


class SignUpActivity : AppCompatActivity() {

    private val mAuth: FirebaseAuth by lazy { FirebaseAuth.getInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        sign_up_activity_arrow_back.setOnClickListener { finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }

        activity_sign_button_login.setOnClickListener {

            val email = signup_editTextEmail.text.toString()
            val password = signup_editTextPassword.text.toString()
            val confirmPassword = signup_editTextConfirmPassword.text.toString()

            if(isValidateEmail(email) && isValidatePassword(password) && isValidateConfirmPassword(password,confirmPassword)){
                signUpByEmail(email,password)
            } else{
             toast("Confirma que todos los datos sean correctos")
             }
        }

        signup_editTextEmail.validate {
            signup_editTextEmail.error = if (isValidateEmail(it)) null else "Correo no valido"
        }
        signup_editTextPassword.validate {
            signup_editTextPassword.error = if(isValidatePassword(it)) null else "Contraseña incorrecta debe contener 1 numero, 1 minuscula, 1 mayuscula, 1 caracter especial y mas de 4 caracteres"
        }
        signup_editTextConfirmPassword.validate {
            signup_editTextConfirmPassword.error = if(isValidateConfirmPassword(signup_editTextPassword.text.toString(),it)) null else "Las contraseñas no son iguales"
        }
    }

    private fun signUpByEmail(email: String, password: String){

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    mAuth.currentUser!!.sendEmailVerification().addOnCompleteListener(this){
                        // Sign in success, update UI with the signed-in user's information
                        toast("Se te a enviado un email, favor de confirmar")
                        goToActivity<JoinActivity> {
                            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        }
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                    }
                } else {
                    // If sign in fails, display a message to the user.
                    toast("Se te a enviado un email, favor de confirmar")
                }
        }
    }


}
