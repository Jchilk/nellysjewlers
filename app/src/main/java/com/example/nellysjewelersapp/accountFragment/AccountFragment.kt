package com.example.nellysjewelersapp.accountFragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.nellysjewelersapp.MainActivity

import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.accountFragment.accountActivity.LoginActivity
import com.example.nellysjewelersapp.accountFragment.accountActivity.ProfileFragment
import com.example.nellysjewelersapp.accountFragment.accountActivity.SignUpActivity
import com.example.nellysjewelersapp.others.preferences

/**
 * A simple [Fragment] subclass.
 */
class AccountFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_account, container, false)

       if(preferences.readLoginStatus()){
           setFragment(ProfileFragment())
        }

        val btnLogin = view.findViewById<Button>(R.id.accout_fragment_login)
        val btnSignup = view.findViewById<Button>(R.id.accout_fragment_sign_up)

        btnLogin.setOnClickListener { intentLoginActivity()}
        btnSignup.setOnClickListener { intentSignupActivity() }

        return view
    }

    private fun intentLoginActivity(){
        val intent = Intent(activity,LoginActivity::class.java)
        intent.putExtra("login","INICIAR SESIÓN")
        startActivity(intent)
    }

    private fun intentSignupActivity(){
        val intent = Intent(activity,SignUpActivity::class.java)
        startActivity(intent)
    }

    private fun setFragment(fragment : Fragment){
        activity!!.supportFragmentManager.beginTransaction().apply {
            replace(R.id.main_activity_container,fragment)
            commit()
        }
    }
}
