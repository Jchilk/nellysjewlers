package com.example.nellysjewelersapp.accountFragment.accountActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.others.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_join.*

class JoinActivity : AppCompatActivity() {

    private val mAuth: FirebaseAuth by lazy { FirebaseAuth.getInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join)

        join_arrowBack.setOnClickListener { finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }

        activity_join_textviewSignUp.setOnClickListener { goToActivity<SignUpActivity>()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }

        join_forgot_password.setOnClickListener { goToActivity<ForgotPasswordActivity>()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }

        activity_join_button_login.setOnClickListener {

            val email = join_editTextEmail.text.toString()
            val password = join_editTextPassword.text.toString()
            if(isValidateEmail(email) && isValidatePassword(password)){
                logInByEmail(email,password)
                preferences.saveStatusLogin(true)
                setFragment(ProfileFragment())
            }else{
                toast("Confirma que todos los datos sean correctos")

            }
        }

        join_editTextEmail.validate {
            join_editTextEmail.error = if (isValidateEmail(it)) null else "Correo no valido"
        }
        join_editTextPassword.validate {
            join_editTextPassword.error = if(isValidatePassword(it)) null else "Contraseña incorrecta debe contener 1 numero, 1 minuscula, 1 mayuscula, 1 caracter especial y mas de 4 caracteres"
        }

    }

    private fun logInByEmail(email: String, password:String){
            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this){ task ->
                if (task.isSuccessful){
                    if(mAuth.currentUser!!.isEmailVerified){
                        toast("Usuario logeado")
                    }else{
                        toast("Confirma primero el correo")
                    }
                }else{
                    toast("Ocurrio un error, intenta de nuevo")
                }
            }
    }

    private fun setFragment(fragment : Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.main_activity_container, fragment)
            commit()
        }
    }

}
