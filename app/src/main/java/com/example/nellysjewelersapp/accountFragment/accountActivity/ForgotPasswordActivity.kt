package com.example.nellysjewelersapp.accountFragment.accountActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.nellysjewelersapp.R
import com.example.nellysjewelersapp.others.goToActivity
import com.example.nellysjewelersapp.others.isValidateEmail
import com.example.nellysjewelersapp.others.toast
import com.example.nellysjewelersapp.others.validate
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : AppCompatActivity() {

    private val mAuth : FirebaseAuth by lazy { FirebaseAuth.getInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        forgot_editTextEmail.validate {
            forgot_editTextEmail.error = if (isValidateEmail(it)) null else "Correo no valido"
        }

        forgot_backArrow.setOnClickListener { finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

        }

        forgot_buttonAcept.setOnClickListener {
            val email = forgot_editTextEmail.text.toString()
            if(isValidateEmail(email)){
                mAuth.sendPasswordResetEmail(email).addOnCompleteListener(this){
                    toast("El correo a sido enviado")
                    goToActivity<JoinActivity>{
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    }
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                }
            }else{
                toast("Asegurate que la dirección de correo sea correcta")
            }
        }

    }
}
